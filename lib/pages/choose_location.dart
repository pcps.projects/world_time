import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';
class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {

  List<WorldTime> locations = [
    WorldTime(flag: 'in', location: 'India', url: 'Asia/Kolkata'),
    WorldTime(flag: 'np', location: 'Nepal', url: 'Asia/Kathmandu'),
    WorldTime(flag: 'gb', location: 'United Kingdom', url: 'Europe/London'),
    WorldTime(flag: 'us', location: 'USA', url:'America/Los_Angeles'),
  ];

  void updateTime(index) async {
    WorldTime worldTime = locations[index];
    await worldTime.getTime();
    // navigate to home screen
    Navigator.pop(context, {
      'location': worldTime.location,
      'time': worldTime.time,
      'flag': worldTime.flag,
      'isDaytime': worldTime.isDayTime,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('Choose a location'),
        centerTitle: true,
        elevation: 0,
      ),
      body: ListView.builder(
          itemCount: locations.length,
          itemBuilder: (context, index){
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 2.0),
              child: Card(
                child: ListTile(
                  onTap: (){
                    updateTime(index);
                  },
                  title: Text(
                      locations[index].location,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                  ),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/flags/${locations[index].flag}.png'),
                  ),
                ),
              ),
            );
          }
      ),
    );
  }
}
