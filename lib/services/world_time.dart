import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {

  String location; // location name for UI
  String time; // Time in that location
  String flag; // Url to an asset flag icon
  String url; // API Url to the given location
  bool isDayTime; // true or false if daytime or not

  WorldTime({this.location, this.flag, this.url});

  // Future is like the promise
  Future<void> getTime() async {
    try{
      // Make api call to server
      Response response = await get('http://worldtimeapi.org/api/timezone/$url');
      Map data = jsonDecode(response.body);
      //get properties from data
      String datetime = data['datetime'];
      int offset = data['raw_offset'];
      // Create a datetime object
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(seconds: offset));
      // set the time property
      time = DateFormat.jm().format(now);
      // set the isDaytime to true or false
      isDayTime = now.hour>6 && now.hour<20 ? true : false;
    }catch(e){
      print('Caught Error: $e');
      time = 'Could not get Time Data';
    }
  }
}